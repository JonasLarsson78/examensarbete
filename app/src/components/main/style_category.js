import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(() => ({
  icon: {
    position: 'relative',
    top: 10,
    marginTop: 10,
    marginLeft: 10,
    marginRight: 5,
  },
}));
