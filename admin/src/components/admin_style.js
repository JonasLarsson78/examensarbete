import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(() => ({
  adminContainer: {
    width: '100vw',
    height: '100vh',
    background: '#99c2ff',
  },
  adminTableContainer: {
    position: 'absolute',
    top: 170,
  },
  adminTable: {
    width: '90vw',
    marginLeft: 10,
    borderCollapse: 'collapse',
  },
  adminModal: {
    width: 300,
    height: 150,
    paddingTop: 25,
    background: 'black',
    color: 'white',
    borderRadius: 5,
    opacity: 0.5,
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%,-50%)',
  },
  adminLoader: {
    textAlign: 'center',
    position: 'relative',
    top: 45,
  },
}));
