import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(() => ({
  footerContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    width: '100vw',
    height: '100px',
    background: '#80b3ff',
  },
  footerHr: {
    width: '98vw',
  },
  footerText: {},
}));
