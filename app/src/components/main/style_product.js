import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(() => ({
  productContainer: {
    position: 'absolute',
    left: 0,
    top: 0,
    width: '100vw',
    height: '100vh',
    background: '#efdebd',
    display: 'flex',
    justifyContent: 'center',
  },
  productItems: {
    flexWrap: 'wrap',
    position: 'relative',
    top: 150,
    width: 500,
    height: 500,
    padding: 30,
    borderRadius: 3,
  },
  productImg: {
    width: 450,
    height: 300,
    borderRadius: 3,
    border: '1px solid black',
  },
  productTitle: {
    fontSize: 30,
    marginBottom: 10,
  },
  productDes: {
    fontSize: 14,
    width: 400,
    marginBottom: 10,
    marginLeft: 5,
  },
  productPrice: {
    fontSize: 16,
    marginBottom: 10,
    marginTop: 5,
    fontWeight: 'bold',
    marginLeft: 5,
  },
  productSeller: {
    fontSize: 14,
    marginBottom: 10,
    marginLeft: 5,
  },
  productPhone: {
    fontSize: 14,
    float: 'left',
    marginRight: 5,
    marginLeft: 5,
  },
  productMail: {
    fontSize: 14,
    marginBottom: 10,
  },
  backLink: {
    textDecoration: 'none',
  },
  link: {
    textDecoration: 'none',
    color: 'black',
    '&hover:': {
      color: 'green',
    },
  },
}));
