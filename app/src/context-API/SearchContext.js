import React, { useState, createContext } from 'react';

export const SearchContext = createContext();

export const SearchProvider = ({ children }) => {
  const [formData, setFormData] = useState({
    search: '',
    category: ' ',
    price: ' ',
    city: ' ',
  });

  return (
    <SearchContext.Provider value={{ formData, setFormData }}>
      {children}
    </SearchContext.Provider>
  );
};
