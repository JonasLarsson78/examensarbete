import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(() => ({
  messageContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100vw',
    height: '100vh',
    background: '#99c2ff',
  },
  messageList: {
    position: 'relative',
    top: 170,
  },
}));
