const express = require('express');
const userIP = require('user-ip');
const port = 3002;
const app = express();
const docs_handler = express.static(__dirname + '/docs/');
let check = true;
const ip = (req, res, next) => {
  let newIp = userIP(req);
  if (check) {
    console.log('Client IP: ', newIp);
  }
  check = false;

  setTimeout(() => {
    check = true;
  }, 1000);

  next();
};

app.use(ip, docs_handler);

app.listen(port, '0.0.0.0', () =>
  console.log(`REST API DOC listening on port ${port}`),
);
