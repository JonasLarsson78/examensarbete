import React from 'react';
import {
  Button,
  Box,
  Table,
  TableHead,
  TableBody,
  TableCell,
  TableRow,
} from '@material-ui/core';

import { postFav } from '../../utils/REST-API';

const Fav = ({ fav, getFavorite, setLoad }) => {
  const sendFav = (e) => {
    setLoad(true);
    const id = e.currentTarget.dataset.id;
    const obj = e.currentTarget.dataset.obj;
    const data = { fav: obj };

    postFav(id, data)
      .then(() => {
        setTimeout(() => {
          getFavorite();
          setLoad(false);
        }, 400);
      })
      .catch((err) => console.log(err));
  };

  const favo = (data) => {
    if (data) {
      return (
        <span style={{ fontSize: 22 }} role="img" aria-label="img">
          ★
        </span>
      );
    } else {
      return (
        <span role="img" aria-label="img" style={{ fontSize: 22 }}>
          ✰
        </span>
      );
    }
  };
  return (
    <Box>
      <Table size="small" className="adminTable">
        <TableHead>
          <TableRow>
            <TableCell>Title</TableCell>
            <TableCell>Category</TableCell>
            <TableCell>Fav</TableCell>
            <TableCell>Action</TableCell>
          </TableRow>
        </TableHead>
        {fav.length ? (
          <TableBody>
            {fav
              .sort((a, b) => JSON.parse(b.fav) - JSON.parse(a.fav))
              .map((data) => {
                return (
                  <TableRow key={data._id}>
                    <TableCell>{data.title}</TableCell>
                    <TableCell>{data.category}</TableCell>
                    <TableCell>{favo(data.fav)}</TableCell>
                    <TableCell>
                      {!data.fav ? (
                        <Button
                          onClick={sendFav}
                          data-id={data._id}
                          data-obj={true}
                        >
                          Add
                        </Button>
                      ) : (
                        <Button
                          onClick={sendFav}
                          data-id={data._id}
                          data-obj={false}
                        >
                          Remove
                        </Button>
                      )}
                    </TableCell>
                  </TableRow>
                );
              })}
          </TableBody>
        ) : (
          <TableBody>
            <TableRow>
              <TableCell style={{ textAlign: 'center' }} colSpan="8">
                List is empty...
              </TableCell>
            </TableRow>
          </TableBody>
        )}
      </Table>
    </Box>
  );
};

export default Fav;
