import {
  Radio as Radios,
  DirectionsBike,
  DriveEta,
  Storefront,
  Deck,
  Home,
  Work,
  Pets,
  Toys,
} from '@material-ui/icons';

export const category = [
  { text: 'Cars', icon: DriveEta },
  { text: 'Clotes', icon: Storefront },
  { text: 'Garden', icon: Deck },
  { text: 'Toys', icon: Toys },
  { text: 'Electronics', icon: Radios },
  { text: 'Home', icon: Home },
  { text: 'Pets', icon: Pets },
  { text: 'Bicycles', icon: DirectionsBike },
  { text: 'Jobs', icon: Work },
].sort((a, b) => {
  return a.text > b.text ? 1 : b.text > a.text ? -1 : 0;
});

export const price = [
  { text: '1kr - 100kr', value: 100 },
  { text: '101kr - 500kr', value: 500 },
  { text: '501kr - 1000kr', value: 1000 },
  { text: '1001kr - 100000kr', value: 100000 },
  { text: '100001kr - 500000kr', value: 500000 },
];

export const city = [
  'Stockholms län',
  'Uppsala län',
  'Södermanlands län',
  'Östergötlands län',
  'Jönköpings län',
  'Kronobergs län',
  'Kalmar län',
  'Gotlands län',
  'Blekinge län',
  'Skåne län',
  'Hallands län',
  'Västra Götalands län',
  'Värmlands län',
  'Örebro län',
  'Västmanlands län',
  'Dalarnas län',
  'Gävleborgs län',
  'Västernorrlands län',
  'Jämtlands län',
  'Västerbottens län',
  'Norrbottens län',
].sort();

export const menyBtn = [
  { name: 'Home', link: '/' },
  { name: 'About', link: '/about' },
  { name: 'Contact', link: '/contact' },
];
