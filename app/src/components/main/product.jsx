import React, { useEffect, useState } from 'react';
import { Box, Button, CardMedia, Link as A } from '@material-ui/core';
import Header from './header';
import Footer from './footer';
import { getAdvert } from '../../utils/REST-API';
import { useStyles } from './style_product';
import { Redirect } from 'react-router-dom';

const Product = ({ match }) => {
  const [product, setProduct] = useState([]);
  const [contact, setContact] = useState(false);
  const [redirect, setRedirect] = useState(false);
  const classes = useStyles();

  const id = match.params.id.substr(1);

  const fixPhoneNumber = (num) => {
    if (num[0] !== '0') {
      let newNum = '0' + num;
      return newNum;
    }

    return num;
  };

  useEffect(() => {
    getAdvert(id)
      .then((res) => {
        setProduct(res.data);
      })
      .catch((err) => console.log(err));
  }, [id]);

  if (redirect) {
    return (
      <Redirect
        to={{
          pathname: '/',
        }}
      />
    );
  }

  return (
    <Box className={classes.productContainer}>
      <Header btn={true} />
      <Box className={classes.productItems}>
        {product.map((data) => {
          return (
            <Box key={data._id}>
              <Box className={classes.productTitle}>
                <b>{data.title}</b>
              </Box>
              <Box className={classes.productSeller}>
                Seller: <i>{data.seller}</i>
              </Box>
              <CardMedia
                className={classes.productImg}
                alt={data.img}
                image={`https://jonas78dev-5798.restdb.io/media/${data.img}`}
              />
              <Box className={classes.productPrice}>
                {'Price: ' + data.price + ' kr'}
              </Box>
              <Box className={classes.productDes}>{data.description}</Box>

              {contact ? (
                <>
                  <Box component="span" className={classes.productPhone}>
                    <b>Phone:</b>{' '}
                    <A
                      className={classes.link}
                      href={`tel:${fixPhoneNumber(data.phone)}`}
                    >
                      {fixPhoneNumber(data.phone)}
                    </A>
                  </Box>
                  <Box className={classes.productMail}>
                    <b>Mail: </b>
                    <A
                      className={classes.link}
                      href={`mailto:${data.mail}?subject=${data.title}`}
                    >
                      {data.mail}
                    </A>
                  </Box>
                </>
              ) : null}

              <Button onClick={() => setContact(!contact)}>
                {!contact ? 'Show Contact' : 'Hide Contact'}
              </Button>

              <Button onClick={() => setRedirect(true)}>Back</Button>
            </Box>
          );
        })}
      </Box>
      <Footer />
    </Box>
  );
};

export default Product;
