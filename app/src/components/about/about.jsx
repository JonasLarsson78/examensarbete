import React, { useContext } from 'react';
import { Typography, Box } from '@material-ui/core';
import { AuthContext } from '../../context-API/AuthContext';

import Header from '../main/header';
import Footer from '../main/footer';

import { useStyles } from './style_about';

import { text } from '../../templates/text';

const About = () => {
  const { auth } = useContext(AuthContext);
  const classes = useStyles();
  return (
    <Box
      className={classes.aboutContantainer}
      display="flex"
      justifyContent="center"
    >
      <Header btn={true} />
      <Box className={classes.aboutItem}>
        <Box>
          <Typography variant="h5">About</Typography>
          <br />
          <Typography>{text.about}</Typography>
          {auth ? (
            <>
              <br />
              <br />
              <Typography variant="h5">About Member</Typography>
              <br />
              <Typography>{text.aboutMember}</Typography>
            </>
          ) : null}
        </Box>
      </Box>
      <Footer />
    </Box>
  );
};

export default About;
