const express = require('express');
const userIP = require('user-ip');
const geoip = require('geoip-lite');
const uuidv4 = require('uuid/v4');
const MongoClient = require('mongodb').MongoClient;
const cors = require('cors');
const multer = require('multer');
const server = express();
const port = 5000;

const {
  URL,
  ADMIN,
  filter,
  filterAll,
  objTemplate,
  urlMongoDB,
  optionsMongoDB,
} = require('./utils/DB_utils');

const upload = multer({
  limits: {
    fileSize: 4 * 1024 * 1024,
  },
});

server.use(express.json());
server.use(cors());

/* ------------- Client IP -------------------- */
server.get(URL.ip, (req, res) => {
  const IP = userIP(req);
  const geo = geoip.lookup(IP);
  geo.ip = IP;
  res.status(200).send(geo);
});

/*------------------------------- Auth for ADMIN -------------------------------*/
// Check if admin
server.post(URL.auth, (req, res) => {
  const { mail } = req.body;
  let check = ADMIN.includes(mail);
  res.status(200).send(check);
});
/* -------------------------------------------------------------------- */

MongoClient.connect(urlMongoDB, optionsMongoDB, (err, db) => {
  if (err) return console.log(err);
  const fleaData = db.db('fleadb').collection('data');
  const fleaMail = db.db('fleadb').collection('mail');
  const fleaImg = db.db('fleadb').collection('img');
  /*------------------------------- Routes for ADMIN ---------------------*/

  // GET all not approved review.
  server.get(URL.advertsAllFalse, (req, res) => {
    fleaData.find({}).toArray((err, data) => {
      const filtered = data.filter((x) => x.ok === false);
      res.status(200).send(filtered);
    });
  });

  // POST approved advert by ID.
  server.post(`${URL.advertURL}/:id`, (req, res) => {
    const { id } = req.params;
    fleaData.findOneAndUpdate(
      { _id: id },
      {
        $set: {
          ok: true,
        },
      },
    );
    res.status(201).end();
  });

  // DELETE not approved advert by ID.
  server.delete(`${URL.advertURL}/:id`, (req, res) => {
    const { id } = req.params;
    fleaData.findOneAndDelete({
      _id: id,
    });
    res.status(204).end();
  });

  // POST true/false to fav.
  server.post(`${URL.advertsFav}/:id`, (req, res) => {
    const { id } = req.params;
    const data = req.body;
    fleaData.findOneAndUpdate(
      { _id: id },
      {
        $set: {
          fav: JSON.parse(data.fav),
        },
      },
    );
    res.status(201).end();
  });

  /* ----------------------------------------------------------------------*/

  /*------------------------------- Routs for APP/ADMIN --------------------*/
  // GET all Favorites
  server.get(URL.advertsFav, (req, res) => {
    let { fav } = req.query;

    if (fav !== undefined) {
      fleaData.find({}).toArray((err, data) => {
        const filtered = filter(fav, data);
        res.status(200).send(filtered);
      });
    } else
      res
        .status(403)
        .send('Must set fav query true or false!!, (/api/fav?fav=true/false)');
  });

  /*------------------------------- Routs for APP ---------------------------*/

  // Get all adverts from DB whit filter.
  server.get(URL.advertsURL, (req, res) => {
    let { search, category, price, city } = req.query;

    fleaData.find({}).toArray((err, data) => {
      res.status(200).send(filterAll(data, search, category, price, city));
    });
  });

  // Get one advert from JSON by ID
  server.get(`${URL.advertURL}/:id`, (req, res) => {
    let { id } = req.params;
    fleaData.find({}).toArray((err, data) => {
      const filtered = data.filter((x) => x._id === id);
      res.status(200).send(filtered);
    });
  });

  // Add new advert
  server.post(URL.advertsURL, (req, res) => {
    const obj = objTemplate(req.body, uuidv4());
    fleaData.insertOne(obj);
    res.status(201).end();
  });
  // GET user ADVERTS
  server.get(`${URL.advertsMail}/:mail`, (req, res) => {
    const { mail } = req.params;
    fleaData.find({}).toArray((err, data) => {
      const filtered = data.filter((x) => x.mail === mail);
      res.status(200).send(filtered);
    });
  });

  /*-------------------------- Contact From Admin------------------------------------*/
  //GET messages.
  server.get(URL.contactFrom, (req, res) => {
    fleaMail.find({}).toArray((err, data) => {
      const n = data.filter((x) => x.new === true);
      const o = data.filter((x) => x.new !== true);

      let json = [{ new: n, old: o }];
      res.status(200).send(json);
    });
  });
  //POST message.
  server.post(URL.contactFrom, (req, res) => {
    const message = req.body;
    message._id = uuidv4();
    message.new = true;
    fleaMail.insertOne(message);
    res.status(201).end();
  });

  //GET Message by ID
  server.get(`${URL.getMess}/:id`, (req, res) => {
    let { id } = req.params;
    fleaMail.find({}).toArray((err, data) => {
      const filtered = data.filter((x) => x._id === id);
      res.status(200).send(filtered);
    });
  });

  //POST Set to old message
  server.post(`${URL.getMess}/:id`, (req, res) => {
    let { id } = req.params;
    fleaMail.findOneAndUpdate(
      { _id: id },
      {
        $set: {
          new: false,
        },
      },
    );
    res.status(201).end();
  });

  // DELETE message
  server.delete(`${URL.getMess}/:id`, (req, res) => {
    let { id } = req.params;
    fleaMail.findOneAndDelete({
      _id: id,
    });
    res.end();
  });

  //---- TEST ZONE - Test rout som inte används. --------------------------------------------
  //-----------------------------------------------------------------------------------------
  server.get(URL.getRandomImg, (req, res) => {
    fleaData.find({}).toArray((err, data) => {
      let imgArr = [];
      data.filter((x) => imgArr.push(x.img));
      let numArr = [];
      while (numArr.length < 4) {
        let r = Math.floor(Math.random() * imgArr.length);
        if (numArr.indexOf(r) === -1) numArr.push(r);
      }
      let slidImgArr = [
        imgArr[numArr[0]],
        imgArr[numArr[1]],
        imgArr[numArr[2]],
        imgArr[numArr[3]],
      ];

      res.status(201).send(slidImgArr);
    });

    server.post('/getimg', (req, res) => {
      const { id } = req.body;

      fleaImg.find({}).toArray((err, data) => {
        const filtered = data.filter((x) => x._id === id);
        res.status(200).send(filtered);
      });
    });

    server.post('/postimg', upload.single('myFile'), (req, res) => {
      let obj = {
        _id: uuidv4(),
        data: req.file,
      };
      fleaImg.insertOne(obj);
      res.status(201).end();
    });
  });
  //--------------------------------------------------------------------------------------------
  //--------------------------------------------------------------------------------------------

  server.listen(port, '0.0.0.0', () =>
    console.log(
      ` Flea Market Server listening on port ${port} ready for bargains!!`,
    ),
  );
});
