import React, { useState, useContext } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { SearchContext } from '../../context-API/SearchContext';
import { AuthContext } from '../../context-API/AuthContext';
import {
  TextField,
  FormLabel,
  Select,
  MenuItem,
  Button,
  Box,
  Typography,
} from '@material-ui/core';
import Header from '../main/header';
import Footer from '../main/footer';
import { postImg, postAdverts } from '../../utils/REST-API';
import { category, city } from '../../templates/category';
import { useStyles } from './style_add';

const Add = () => {
  const [redirect, setRedirect] = useState(false);
  const [errMess, setErrMess] = useState({ text: '', color: 'black' });
  const [formObj, setFormObj] = useState({
    title: '',
    description: '',
    price: 0,
    seller: '',
    mail: '',
    phone: '',
    img: '',
    category: '',
    city: '',
  });
  const { formData } = useContext(SearchContext);
  const { auth, mail } = useContext(AuthContext);
  const classes = useStyles();

  const onChangeForm = (e) => {
    setFormObj({ ...formObj, [e.target.name]: e.target.value });
  };

  const addData = () => {
    let imagefile = document.querySelector('#file').files[0];

    if (
      formObj.title &&
      formObj.description &&
      formObj.price !== 0 &&
      formObj.seller &&
      formObj.phone &&
      formObj.category &&
      formObj.city &&
      imagefile
    ) {
      let formData = new FormData();
      formData.append('image', imagefile, imagefile.name);

      postImg(formData)
        .then((res) => {
          const id = res.data.ids[0];
          formObj.img = id;
          formObj.mail = mail;
        })
        .then(() => {
          postAdverts(formObj).then(() => {
            setErrMess({
              ...errMess,
              color: 'black',
              text: 'Your ad has been added for review.',
            });
            setTimeout(() => {
              setRedirect(true);
            }, 3000);
          });
        })
        .catch((err) => {
          console.log(err);
        });
    } else {
      setErrMess({
        ...errMess,
        color: 'red',
        text: 'Must fill in all fields!!',
      });
      setTimeout(() => {
        setErrMess({
          ...errMess,
          color: 'black',
          text: '',
        });
      }, 3000);
    }
  };

  if (redirect || !auth) {
    return <Redirect to="/" />;
  }

  return (
    <Box className={classes.addContainer}>
      <Header btn={true} />

      <Box className={classes.addFormContainer}>
        <FormLabel>Title </FormLabel>
        <br />
        <TextField
          className={classes.textField}
          onChange={onChangeForm}
          name="title"
          type="text"
          variant="outlined"
          size="small"
        />
        <br />
        <FormLabel>Description </FormLabel>
        <br />
        <TextField
          multiline
          className={classes.textField}
          onChange={onChangeForm}
          name="description"
          type="text"
          variant="outlined"
          size="small"
        />
        <br />

        <FormLabel>Seller </FormLabel>
        <br />
        <TextField
          className={classes.textField}
          onChange={onChangeForm}
          name="seller"
          type="text"
          variant="outlined"
          size="small"
        />
        <br />
        <FormLabel>Price </FormLabel>
        <br />
        <TextField
          className={classes.textField}
          onChange={onChangeForm}
          name="price"
          type="number"
          variant="outlined"
          size="small"
        />
        <br />
        <FormLabel>Mail </FormLabel>
        <br />
        <Typography>{mail}</Typography>
        <br />
      </Box>
      <Box className={classes.addFormContainer}>
        <FormLabel>Phone </FormLabel>
        <br />
        <TextField
          className={classes.textField}
          onChange={onChangeForm}
          name="phone"
          type="number"
          variant="outlined"
          size="small"
        />
        <br />
        <FormLabel>Category </FormLabel>
        <br />
        <Select
          onChange={onChangeForm}
          defaultValue={formData.category}
          className={classes.textField}
          name="category"
          style={{ height: 40 }}
          variant="outlined"
        >
          <MenuItem value=" ">
            <em>-- Select an Category --</em>
          </MenuItem>
          {category.map((str, index) => (
            <MenuItem key={index} value={str.text}>
              {str.text}
            </MenuItem>
          ))}
        </Select>
        <br />
        <FormLabel>County </FormLabel>
        <br />
        <Select
          defaultValue={formData.city}
          onChange={onChangeForm}
          className={classes.textField}
          name="city"
          style={{ height: 40 }}
          variant="outlined"
        >
          <MenuItem value=" ">
            <em>-- Select an County --</em>
          </MenuItem>
          {city.map((str) => (
            <MenuItem key={str} value={str}>
              {str}
            </MenuItem>
          ))}
        </Select>
        <br />
        <FormLabel>Image </FormLabel>
        <br />
        <TextField
          accept="image/*"
          id="file"
          type="file"
          name="img"
          variant="outlined"
          className={classes.textField}
          size="small"
        />
        <br />
        <Button className={classes.addInputAddBtn} onClick={addData}>
          Add Flea
        </Button>
        <Link className={classes.addBtnBack} to="/">
          <Button>Back</Button>
        </Link>
        <Box style={{ marginLeft: 5, color: errMess.color, fontSize: 14 }}>
          {errMess.text}
        </Box>
      </Box>

      <Footer />
    </Box>
  );
};

export default Add;
