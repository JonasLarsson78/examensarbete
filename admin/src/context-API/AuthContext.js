import React, { useState, createContext } from 'react';

export const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
  const [auth, setAuth] = useState(false);
  const [mess, setMess] = useState(false);

  return (
    <AuthContext.Provider value={{ auth, setAuth, mess, setMess }}>
      {children}
    </AuthContext.Provider>
  );
};
