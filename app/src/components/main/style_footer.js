import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(() => ({
  footerContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    width: '100%',
    height: 100,
    background: '#e2ca9a',
  },

  footerText: {
    textAlign: 'center',
    paddingTop: 30,
    color: '#634b01',
  },
}));
