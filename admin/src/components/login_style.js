import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(() => ({
  loginContainer: {
    width: '100vw',
    height: '100vh',
    background: '#80b3ff',
  },
  loginCard: {
    marginTop: 150,
    padding: 20,
    height: 500,
    background: 'white',
  },
  loginCardMedia: {
    height: 30,
    width: 400,
    paddingTop: '56.25%',
  },
  textField: {
    '& label.Mui-focused': {
      color: '#634b01',
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: '#634b01',
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: 'red',
      },
      '&:hover fieldset': {
        borderColor: '#634b01',
      },
      '&.Mui-focused fieldset': {
        borderColor: '#634b01',
        border: '1px solid',
      },
    },
  },
}));
