import { BehaviorSubject } from 'rxjs';

/* --------------------------------------------------- */

export const auth$ = new BehaviorSubject(
  window.localStorage.getItem('auth') || false,
);

export function setAuth$(newAuth) {
  if (!newAuth) {
    window.localStorage.removeItem('auth');
  } else {
    window.localStorage.setItem('auth', newAuth);
  }

  auth$.next(newAuth);
}

/* ---------------------------------------------------- */
export const mail$ = new BehaviorSubject(
  window.localStorage.getItem('mail') || '',
);

export function setMail$(newMail) {
  if (!newMail) {
    window.localStorage.removeItem('mail');
  } else {
    window.localStorage.setItem('mail', newMail);
  }

  mail$.next(newMail);
}
/* ---------------------------------------------------- */
export const location$ = new BehaviorSubject(
  window.localStorage.getItem('location') || null,
);

export function setlocation$(newLocation) {
  if (!newLocation) {
    window.localStorage.removeItem('location');
  } else {
    window.localStorage.setItem('location', newLocation);
  }

  location$.next(newLocation);
}
