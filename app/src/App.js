import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { SearchProvider } from '../src/context-API/SearchContext';
import { ModalProvider } from '../src/context-API/ModalContext';

import Main from './components/main/main';
import Add from './components/add/add';
import Product from './components/main/product';
import My from './components/myFlea/my';
import About from './components/about/about';
import Contact from './components/contact/contact';

import './App.css';

function App() {
  return (
    <SearchProvider>
      <ModalProvider>
        <Router>
          <Route exact path="/" component={Main} />
          <Route path="/about" component={About} />
          <Route path="/contact" component={Contact} />
          <Route path="/product:id" component={Product} />
          <Route path="/add" component={Add} />
          <Route path="/my:mail" component={My} />
        </Router>
      </ModalProvider>
    </SearchProvider>
  );
}

export default App;
