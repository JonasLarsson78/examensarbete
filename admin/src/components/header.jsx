import React, { useContext, useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Button, Box, Badge } from '@material-ui/core';
import { logOut } from '../firebase/firebaseUtils';
import { AuthContext } from '../context-API/AuthContext';

import { getNewMess } from '../utils/REST-API';

import { useStyles } from './header_style';

const Header = ({ btn, get, favorite, setFavorite }) => {
  const [mail, setMail] = useState([]);
  const { setAuth } = useContext(AuthContext);
  const classes = useStyles();

  useEffect(() => {
    getNewMess()
      .then((res) => {
        setMail(res.data[0].new);
      })
      .catch((err) => console.log(err));
  }, []);

  return btn ? (
    <Box className={classes.headerContainer}>
      <img
        alt="logo"
        className={classes.headerLogo}
        src="../images/flea-market-logo.jpg"
      />
      <Button
        onClick={() => setFavorite(!favorite)}
        className={classes.headerUpdateBtn}
        style={!favorite ? { right: 230 } : null}
      >
        {!favorite ? 'Set Favorite' : 'Get To List'}
      </Button>
      {!favorite ? (
        <Button onClick={() => get()} className={classes.headerUpdateBtn}>
          Update List...
        </Button>
      ) : null}
      <Link to="/list">
        <Button className={classes.headerMessBtn}>
          <Badge
            badgeContent={mail.length}
            color="error"
            className={classes.padding}
            showZero={true}
          >
            Messages
          </Badge>
        </Button>
      </Link>
      <Button
        onClick={() => logOut().then(() => setAuth(false))}
        className={classes.headerLogoutBtn}
      >
        Logout
      </Button>
    </Box>
  ) : (
    <Box className={classes.headerContainer}>
      <img
        alt="logo"
        className={classes.headerLogo}
        src="../images/flea-market-logo.jpg"
      />
    </Box>
  );
};

export default Header;
