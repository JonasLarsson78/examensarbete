import React, { useContext, useEffect, useState, useCallback } from 'react';
import { Link } from 'react-router-dom';
import { AuthContext } from '../../context-API/AuthContext';
import { Redirect } from 'react-router-dom';
import {
  Table,
  TableHead,
  TableCell,
  TableRow,
  TableBody,
  Button,
  Box,
} from '@material-ui/core';
import { getMail, delFlea, delImg } from '../../utils/REST-API';

import Header from '../main/header';
import Footer from '../main/footer';
import { useStyles } from './style_my';

const My = ({ match }) => {
  const [list, setList] = useState([]);
  const { auth, mail } = useContext(AuthContext);
  const loginMail = match.params.mail.substr(1);

  const classes = useStyles();

  const get = useCallback(() => {
    getMail(mail).then((res) => {
      setList(res.data);
    });
  }, [mail]);

  useEffect(() => {
    if (loginMail === mail) {
      get();
    }
  }, [loginMail, mail, get]);

  const del = (e) => {
    const { id, img } = e.currentTarget.dataset;

    delFlea(id)
      .then(() => {
        get();
      })
      .then(() => {
        delImg(img);
      });
  };

  if (!auth) {
    return <Redirect to="/" />;
  }

  return (
    <Box className={classes.myContainer}>
      <Header btn={true} />
      <Box className={classes.myItem}>
        <Table className={classes.myTable} size="small">
          <TableHead>
            <TableRow>
              <TableCell>Title</TableCell>
              <TableCell>Category</TableCell>
              <TableCell>Delete</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {list
              .sort((a, b) => {
                return a.category > b.category
                  ? 1
                  : b.category > a.category
                  ? -1
                  : 0;
              })
              .map((data) => {
                return (
                  <TableRow key={data._id}>
                    <TableCell>
                      <Link
                        className={classes.myLink}
                        to={`/product:${data._id}`}
                      >
                        {data.title}
                      </Link>
                    </TableCell>
                    <TableCell>{data.category}</TableCell>
                    <TableCell>
                      <Button
                        onClick={del}
                        data-id={data._id}
                        data-img={data.img}
                      >
                        Delete
                      </Button>
                    </TableCell>
                  </TableRow>
                );
              })}
            <TableRow>
              <TableCell align="right" colSpan="3">
                <Link className={classes.myLinkBtn} to="/">
                  <Button>Back</Button>
                </Link>
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </Box>
      <Footer />
    </Box>
  );
};

export default My;
