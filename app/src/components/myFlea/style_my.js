import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(() => ({
  myContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    backgroundColor: '#efdebd',
    width: '100vw',
    height: '100vh',
  },
  myItem: {
    position: 'relative',
    top: 170,
    padding: 10,
  },
  myTable: {
    backgroundColor: '#e2ca9a',
    borderRadius: 4,
  },
  myLinkBtn: {
    textDecoration: 'none',
  },
  myLink: {
    textDecoration: 'none',
    color: '#634b01',
    fontWeight: 'bold',
    '&:hover': {
      color: 'black',
    },
  },
}));
