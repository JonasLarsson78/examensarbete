import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(() => ({
  loginModal: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100vw',
    height: '100vh',
    background: '#0000006b',
    zIndex: 10000,
    justifyContent: 'center',
  },
  loginCard: {
    position: 'relative',
    top: 300,
    width: 400,
    height: 250,
    padding: 30,
    backgroundColor: 'white',
    border: '1px solid black',
  },
  textField: {
    marginTop: 1,
    backgroundColor: 'white',
    borderRadius: 4,
    '& label.Mui-focused': {
      color: '#634b01',
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: '#634b01',
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        //borderColor: 'red',
      },
      '&:hover fieldset': {
        borderColor: '#634b01',
      },
      '&.Mui-focused fieldset': {
        borderColor: '#634b01',
        border: '1px solid',
      },
    },
  },
}));
