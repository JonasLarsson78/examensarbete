import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(() => ({
  messageContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100vw',
    height: '100vh',
    background: '#99c2ff',
  },
  itemContainer: {
    position: 'relative',
    top: 170,
    padding: 20,
  },
  tableHeadCell: {
    fontWeight: 'bold',
  },
  iconPos: {
    position: 'relative',
    top: 6,
  },
  tableBodyCell: {
    paddingLeft: 45,
  },
  ReactCountryFlag1: {
    position: 'relative',
    top: -1.5,
    left: -5,
    width: 20,
    height: 20,
  },
  ReactCountryFlag2: {
    position: 'relative',
    top: -1.5,
    left: 5,
    width: 20,
    height: 20,
  },
  spanHover: {
    cursor: 'pointer',
  },
  mapLink: {
    textDecoration: 'none',
    color: 'black',
  },
  backBtn: {
    textDecoration: 'none',
    position: 'relative',
    top: 20,
    left: 10,
  },
  resBtn: {
    textDecoration: 'none',
    position: 'relative',
    top: 20,
    left: 5,
  },
  infoModal: {
    position: 'absolute',
    top: 350,
    left: '50%',
    transform: 'translate(-50%, 0px)',
    width: 380,
    height: 120,
    border: '1px solid black',
    borderRadius: 4,
    zIndex: 10,
  },
  infoBack: {
    position: 'fixed',
    top: 0,
    width: '100%',
    height: '100%',
    background: 'white',
    opacity: 0.9,
    zIndex: -1,
    borderRadius: 4,
  },
  bName: {
    marginLeft: 40,
  },
}));
