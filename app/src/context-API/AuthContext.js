import React, { useState, createContext, useEffect } from 'react';
import {
  auth$,
  setAuth$,
  mail$,
  setMail$,
  location$,
  setlocation$,
} from './localStorage';

const onLoad = (f) => {
  let loadTime = new Date();
  let unloadTime = new Date(JSON.parse(window.localStorage.unloadTime));
  let refreshTime = loadTime.getTime() - unloadTime.getTime();

  if (refreshTime > 3000) {
    window.localStorage.clear();
    f('You must login again!!');
  }
};

export const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
  const [loginTime, setLoginTime] = useState('');
  const [auth, setAuth] = useState(false);
  const [mail, setMail] = useState('');
  const [location, setlocation] = useState(null);

  const logOutStore = () => {
    setAuth$(false);
    setMail$('');
    setlocation$(null);
  };

  useEffect(() => {
    if (auth) {
      setAuth$(auth);
    }
    if (auth$.value) {
      setAuth(auth$.value);
    }
  }, [auth]);

  useEffect(() => {
    if (mail !== '') {
      setMail$(mail);
    }
    if (mail$.value.length > 0) {
      setMail(mail$.value);
    }
  }, [mail]);

  useEffect(() => {
    if (location !== null) {
      setlocation$(location);
    }
    if (location$.value !== null) {
      setlocation(location$.value);
    }
  }, [location]);

  if (auth) {
    window.onbeforeunload = () => {
      window.localStorage.unloadTime = JSON.stringify(new Date());
    };

    window.onload = () => {
      onLoad(setLoginTime);
    };
  }

  return (
    <AuthContext.Provider
      value={{
        auth,
        setAuth,
        mail,
        setMail,
        location,
        setlocation,
        logOutStore,
        loginTime,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};
