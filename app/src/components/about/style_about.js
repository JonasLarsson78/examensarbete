import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(() => ({
  aboutContantainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100vw',
    height: '100vh',
    background: '#efdebd',
  },
  aboutItem: {
    position: 'relative',
    width: 500,
    marginTop: 200,
  },
}));
