import { makeStyles, withStyles, InputBase } from '@material-ui/core';

export const Input = withStyles((theme) => ({
  input: {
    width: 260,
    borderRadius: 4,
    backgroundColor: 'white',
    border: '1px solid gray',
    fontSize: 16,
    padding: '10px 26px 10px 12px',

    '&:focus': {
      borderRadius: 4,
      borderColor: '#634b01',
      backgroundColor: 'white',
    },
  },
}))(InputBase);

export const useStyles = makeStyles(() => ({
  textField: {
    width: 260,
    marginTop: 1,
    backgroundColor: 'white',
    borderRadius: 4,
    '& label.Mui-focused': {
      //color: '#634b01',
    },
    '& .MuiInput-underline:after': {
      //borderBottomColor: '#634b01',
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        //borderColor: 'red',
      },
      '&:hover fieldset': {
        borderColor: '#634b01',
      },
      '&.Mui-focused fieldset': {
        borderColor: '#634b01',
        border: '1px solid',
      },
    },
  },
  searchContainer: {
    position: 'relative',
    width: '100vw',
    top: 170,
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap',
    verticalAlign: 'middle',
    background: '#efdebd',
  },
  icon: {
    marginLeft: 10,
    marginRight: 5,
    marginTop: 5,
  },
  clearBtn: {
    marginLeft: 10,
  },
}));
