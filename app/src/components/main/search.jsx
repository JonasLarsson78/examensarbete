import React, { useContext } from 'react';
import { price, city } from '../../templates/category';
import { SearchContext } from '../../context-API/SearchContext';
import {
  LocalOffer as LocalOfferIcon,
  Search as SearchIcon,
  LocationCity as LocationCityIcon,
} from '@material-ui/icons';

import {
  TextField,
  Container,
  Select,
  MenuItem,
  Button,
} from '@material-ui/core';

import { useStyles, Input } from './style_search';
import Category from './category';

const Search = () => {
  const classes = useStyles();
  const { formData, setFormData } = useContext(SearchContext);

  const onChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };
  const clear = () => {
    setFormData({
      ...formData,
      search: '',
      category: ' ',
      price: ' ',
      city: ' ',
    });
  };
  return (
    <Container className={classes.searchContainer}>
      <SearchIcon
        htmlColor="#634b01"
        fontSize="large"
        className={classes.icon}
      />
      <TextField
        value={formData.search}
        variant="outlined"
        name="search"
        size="small"
        className={classes.textField}
        input={<Input />}
        type="text"
        label="Search"
        onChange={onChange}
      />
      <LocalOfferIcon
        htmlColor="#634b01"
        fontSize="large"
        className={classes.icon}
      />
      <Select
        value={formData.price}
        onChange={onChange}
        input={<Input />}
        name="price"
      >
        <MenuItem value=" ">
          <em>-- Select an Price --</em>
        </MenuItem>
        {price.map((str, index) => (
          <MenuItem key={index} value={str.value}>
            {str.text}
          </MenuItem>
        ))}
      </Select>
      <LocationCityIcon
        htmlColor="#634b01"
        fontSize="large"
        className={classes.icon}
      />
      <Select
        value={formData.city}
        variant="outlined"
        onChange={onChange}
        input={<Input />}
        name="city"
      >
        <MenuItem value=" ">
          <em>-- Select an County --</em>
        </MenuItem>
        {city.map((str) => (
          <MenuItem key={str} value={str}>
            {str}
          </MenuItem>
        ))}
      </Select>
      <Button className={classes.clearBtn} onClick={clear}>
        Clear
      </Button>
      <Category />
    </Container>
  );
};

export default Search;
