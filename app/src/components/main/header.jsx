import React, { useContext } from 'react';
import { logOut } from '../../firebase/firebaseUtils';
import { AuthContext } from '../../context-API/AuthContext';
import { ModalContext } from '../../context-API/ModalContext';
import { Box, CardMedia, Button } from '@material-ui/core';
import { Link } from 'react-router-dom';
import { menyBtn } from '../../templates/category';

import LoginModal from './login';

import { useStyles } from './style_header';

const Header = ({ btn }) => {
  const { auth, setAuth, mail, setMail, logOutStore } = useContext(AuthContext);
  const { setShowModal } = useContext(ModalContext);
  const classes = useStyles();

  const logout = () => {
    logOut()
      .then(() => {
        logOutStore();
        setAuth(false);
        setMail('');
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const signIn = (text, mail) => (
    <>
      <span>
        {text}
        <b>{mail}</b>{' '}
      </span>
    </>
  );

  return (
    <>
      <LoginModal />
      <Box className={classes.headerContainer}>
        <CardMedia
          alt="logo"
          className={classes.headerLogo}
          image="../images/flea-market-logo_color.jpg"
        />

        {btn ? (
          <>
            <Box
              display="flex"
              justifyContent="flex-end"
              className={classes.headerNav}
            >
              {menyBtn.map((b, index) => {
                return (
                  <Link
                    key={index}
                    to={b.link}
                    className={classes.headerNoneTextDecoration}
                  >
                    <Button>{b.name}</Button>
                  </Link>
                );
              })}

              {auth ? (
                <>
                  <Link
                    className={classes.headerNoneTextDecoration}
                    to={`/my:${mail}`}
                  >
                    <Button>My Fleas</Button>
                  </Link>
                  <Link className={classes.headerNoneTextDecoration} to="/add">
                    <Button>Add Flea</Button>
                  </Link>
                </>
              ) : null}
            </Box>
            <Button
              onClick={!auth ? () => setShowModal('flex') : logout}
              className={classes.headerLogOutBtn}
            >
              {!auth ? 'Login' : 'LogOut'}
            </Button>
            <Button className={classes.headerLoginBtn} children disabled={true}>
              {!auth ? '' : signIn('Sign In As: ', mail)}
            </Button>
          </>
        ) : null}
      </Box>
    </>
  );
};

export default Header;
