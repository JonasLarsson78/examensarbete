import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(() => ({
  addContainer: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    background: '#efdebd',
    display: 'flex',
    justifyContent: 'center',
  },
  addFormContainer: {
    position: 'relative',
    top: 170,
    height: 0,
    padding: 10,
    background: '#efdebd',
  },

  textField: {
    width: 450,
    marginBottom: 10,
    backgroundColor: 'white',
    borderRadius: 4,
    '& label.Mui-focused': {
      color: '#634b01',
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: '#634b01',
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        //borderColor: 'red',
      },
      '&:hover fieldset': {
        borderColor: '#634b01',
      },
      '&.Mui-focused fieldset': {
        borderColor: '#634b01',
        border: '1px solid',
      },
    },
  },
  textArea: {
    width: 450,
    height: 50,
    marginBottom: 10,
    backgroundColor: 'white',
    borderRadius: 4,
    '& label.Mui-focused': {
      //color: '#634b01',
    },
    '& .MuiInput-underline:after': {
      //borderBottomColor: '#634b01',
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        //borderColor: 'red',
      },
      '&:hover fieldset': {
        borderColor: '#634b01',
      },
      '&.Mui-focused fieldset': {
        borderColor: '#634b01',
        border: '1px solid',
      },
    },
  },
  addBtnBack: {
    textDecoration: 'none',
  },
}));
