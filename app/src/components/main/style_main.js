import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(() => ({
  mainContainer: {
    position: 'absolute',
    left: 0,
    top: 0,
    width: '100vw',
    height: '100vh',
    background: '#efdebd',
  },
  loginTimeContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: 'calc(100% - 4px)',
    height: 'calc(100% - 4px)',
    background: 'black',
    opacity: 0.5,
    border: '2px solid black',
    zIndex: 5000,
  },
  loginTimeItem: {
    position: 'absolute',
    top: 200,
    left: '50%',
    transform: 'translate(-50%, 0px)',
    fontWeight: 'bold',
    background: 'white',
    zIndex: 10000,
    width: 300,
    height: 100,
    borderRadius: 4,
    textAlign: 'center',
    paddingTop: 30,
  },
}));
