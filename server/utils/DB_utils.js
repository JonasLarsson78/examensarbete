/*----------------- Utils to MongoDB ------------------*/
const urlMongoDB =
  'mongodb+srv://jonas:passtomydb@fleamarketdb-fpraw.mongodb.net/test?retryWrites=true&w=majority';
exports.urlMongoDB = urlMongoDB;

const optionsMongoDB = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
};
exports.optionsMongoDB = optionsMongoDB;
/* ---------------------------------------------------- */

/* ------------------Utils to REST API ----------------- */
// All url:s
const URL = {
  auth: '/auth',
  ip: '/ip',
  advertURL: '/api/advert',
  advertsURL: '/api/adverts',
  advertsAllFalse: '/api/allfalse',
  advertsFav: '/api/fav',
  advertsMail: '/api/mail',
  contactFrom: '/api/contact',
  getMess: '/api/mess',
  getRandomImg: '/api/imgrandom', //används inte just nu
};
exports.URL = URL;

// ADMIN Users.
const ADMIN = ['admin@test.se'];
exports.ADMIN = ADMIN;

const objTemplate = (obj, id) => {
  let objTemp = {
    _id: id,
    title: obj.title,
    description: obj.description,
    price: Number(obj.price),
    seller: obj.seller,
    mail: obj.mail,
    category: obj.category,
    ok: false,
    phone: Number(obj.phone),
    img: obj.img,
    fav: false,
    city: obj.city,
  };
  return objTemp;
};
exports.objTemplate = objTemplate;

const filter = (bool, arr) => {
  if (bool === 'all') {
    return arr;
  } else {
    const fav = arr.filter((x) => x.fav === JSON.parse(bool));
    return fav;
  }
};

exports.filter = filter;

// Is number (n) between a and b.
const isBetween = (n, a, b) => {
  return (n - a) * (n - b) <= 0;
};

// Check IFbetween price.
const ifBetween = (price, p) => {
  if (price === '100') {
    return isBetween(p, 1, 100);
  } else if (price === '500') {
    return isBetween(p, 101, 500);
  } else if (price === '1000') {
    return isBetween(p, 501, 1000);
  } else if (price === '100000') {
    return isBetween(p, 1001, 100000);
  } else {
    return isBetween(p, 100001, 500000);
  }
};

// Filter all querys (search, category, price, city).
const filterAll = (db, search, category, price, city) => {
  const filtered = db.filter((x) => {
    if (category === ' ') {
      category = '';
    }
    if (price === ' ') {
      price = '';
    }
    if (search && category && price && city && x.ok === true) {
      return (
        x.title.toLowerCase().includes(search.toLowerCase()) &&
        x.category.includes(category) &&
        x.city.includes(city) &&
        ifBetween(price, x.price)
      );
    } else if (search && category && price && x.ok === true) {
      return (
        x.title.toLowerCase().includes(search.toLowerCase()) &&
        x.category.includes(category) &&
        ifBetween(price, x.price)
      );
    } else if (price && city && category && x.ok === true) {
      return (
        x.category.includes(category) &&
        x.city.includes(city) &&
        ifBetween(price, x.price)
      );
    } else if (search && city && price && x.ok === true) {
      return (
        x.title.toLowerCase().includes(search.toLowerCase()) &&
        x.city.includes(city) &&
        ifBetween(price, x.price)
      );
    } else if (search && city && category && x.ok === true) {
      return (
        x.title.toLowerCase().includes(search.toLowerCase()) &&
        x.category.includes(category) &&
        x.city.includes(city)
      );
    } else if (search && price && x.ok === true) {
      return (
        x.title.toLowerCase().includes(search.toLowerCase()) &&
        ifBetween(price, x.price)
      );
    } else if (category && price && x.ok === true) {
      return x.category.includes(category) && ifBetween(price, x.price);
    } else if (search && category && x.ok === true) {
      return (
        x.title.toLowerCase().includes(search.toLowerCase()) &&
        x.category.includes(category)
      );
    } else if (search && city && x.ok === true) {
      return (
        x.title.toLowerCase().includes(search.toLowerCase()) &&
        x.city.includes(city)
      );
    } else if (category && city && x.ok === true) {
      return x.category.includes(category) && x.city.includes(city);
    } else if (price && city && x.ok === true) {
      return x.city.includes(city) && ifBetween(price, x.price);
    } else if (search && x.ok === true) {
      return x.title.toLowerCase().includes(search.toLowerCase());
    } else if (category && x.ok === true) {
      return x.category.includes(category);
    } else if (price && x.ok === true) {
      return ifBetween(price, x.price);
    } else if (city && x.ok === true) {
      return x.city.includes(city);
    }
  });
  return filtered;
};

exports.filterAll = filterAll;
