import React, { useState, useEffect } from 'react';
import {
  Box,
  Button,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Divider,
} from '@material-ui/core';
import { Inbox, Drafts } from '@material-ui/icons';
import { Link } from 'react-router-dom';

import { getNewMess } from '../../utils/REST-API';

import Header from '../header';

import { useStyles } from './style_messageList';

const MessageList = () => {
  const [listOld, setListOld] = useState([]);
  const [listNew, setListNew] = useState([]);

  const classes = useStyles();

  useEffect(() => {
    getNewMess()
      .then((res) => {
        setListNew(res.data[0].new);
        setListOld(res.data[0].old);
      })
      .catch((err) => console.log(err));

    return () => {};
  }, []);

  return (
    <Box className={classes.messageContainer}>
      <Header btn={false} />
      <Box className={classes.messageList}>
        <List style={{ padding: 30 }} component="nav" aria-label="contacts">
          <ListItem>
            <ListItemIcon>
              <Inbox />
            </ListItemIcon>
            <ListItemText primary="New Message" />
          </ListItem>
          <Divider />
          {listNew.map((data) => {
            return (
              <ListItem
                component={Link}
                to={{
                  pathname: `/message:${data._id}`,
                }}
                key={data._id}
                button
              >
                <ListItemText inset primary={data.name + ' - ' + data.mail} />
              </ListItem>
            );
          })}
        </List>
        <Divider />
        <List style={{ padding: 30 }} component="nav" aria-label="contact">
          <ListItem>
            <ListItemIcon>
              <Drafts />
            </ListItemIcon>
            <ListItemText primary="Message" />
          </ListItem>
          <Divider />
          {listOld.map((data) => {
            return (
              <ListItem
                component={Link}
                to={{
                  pathname: `/message:${data._id}`,
                }}
                key={data._id}
                button
              >
                <ListItemText inset primary={data.name + ' - ' + data.mail} />
              </ListItem>
            );
          })}
        </List>
        <Divider />
        <Box style={{ padding: 90 }}>
          <Link style={{ textDecoration: 'none' }} to="/">
            <Button>Back</Button>
          </Link>
        </Box>
      </Box>
    </Box>
  );
};

export default MessageList;
