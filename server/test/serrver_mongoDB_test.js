const chai = require('chai');
const chaiHttp = require('chai-http');

const { URL } = require('../utils/DB_utils');

chai.use(chaiHttp);
chai.should();

// Tänk på att servern måste vara startad "nodemon server_mongoDB.js"

describe('Get list off all fleas to review.', () => {
  describe('GET /api/allfalse', () => {
    it('Should get all new false.', (done) => {
      chai
        .request('http://localhost:5000')
        .get(URL.advertsAllFalse)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('array');
          done();
        });
    });
  });
});
