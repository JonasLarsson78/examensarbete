import axios from 'axios';

const auth = '/auth';
const advertURL = '/api/advert';
const advertsFav = '/api/fav';
const contactFrom = '/api/contact';
const getM = '/api/mess';
const advertsAllFalse = '/api/allfalse';

export const checkAdmin = (mail) => axios.post(auth, mail);

export const getFav = () => axios.get(`${advertsFav}?fav=all`);
export const postFav = (id, obj) => axios.post(`${advertsFav}/${id}`, obj);

export const udateFromDB = () => axios.post('/db');

export const getAllFalse = () => axios.get(advertsAllFalse);

export const delFlea = (id) => axios.delete(`${advertURL}/${id}`);
export const postTrue = (id) => axios.post(`${advertURL}/${id}`);
export const getNewMess = () => axios.get(contactFrom);
export const getMess = (id) => axios.get(`${getM}/${id}`);
export const postMessOld = (id) => axios.post(`${getM}/${id}`);
export const delMess = (id) => axios.delete(`${getM}/${id}`);

export const delImg = (id) =>
  axios.delete(`https://jonas78dev-5798.restdb.io/media/${id}`, {
    headers: {
      'x-apikey': '5e1c819d4cfae7143cefb101',
    },
  });
