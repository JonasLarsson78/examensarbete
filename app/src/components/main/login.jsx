import React, { useState, useContext, useEffect } from 'react';
import {
  createWithPassword,
  authWithPassword,
  init,
} from '../../firebase/firebaseUtils';
import { AuthContext } from '../../context-API/AuthContext';
import { ModalContext } from '../../context-API/ModalContext';
import {
  Button,
  Box,
  Card,
  CardActions,
  TextField,
  Typography,
} from '@material-ui/core';
import { useStyles } from './style_login';

const LoginModal = () => {
  const [inputData, setInputData] = useState({ email: '', password: '' });
  const [rewPassword, setRewPassword] = useState({ rewpassword: '' });
  const [newUser, setNewUser] = useState(false);
  const { setAuth, setMail } = useContext(AuthContext);
  const { showModal, setShowModal } = useContext(ModalContext);
  const classes = useStyles();

  useEffect(() => {
    init();
  }, []);

  const updateData = (e, obj, updateObj) => {
    updateObj({ ...obj, [e.target.name]: e.target.value });
  };

  const cancel = () => {
    setShowModal('none');
    setNewUser(false);
  };
  const login = () => {
    authWithPassword(inputData)
      .then((res) => {
        setAuth(true);
        setMail(res.user.email);
        cancel();
      })
      .catch((e) => console.log(e.message));
  };
  const addUser = () => {
    if (
      inputData.password === rewPassword.rewpassword &&
      inputData.password !== ''
    ) {
      createWithPassword(inputData)
        .then((res) => {
          setAuth(true);
          setMail(res.user.email);
          cancel();
        })
        .catch((e) => console.log(e.message));
    } else {
      console.log('password not match..');
    }
  };
  const toggelUser = () => {
    setNewUser(!newUser);
  };

  return (
    <Box style={{ display: showModal }} className={classes.loginModal}>
      <Card className={classes.loginCard}>
        <Typography style={{ marginBottom: 25 }}>Login</Typography>
        <TextField
          fullWidth
          label="E-mail"
          name="email"
          onChange={(e) => updateData(e, inputData, setInputData)}
          type="text"
          size="small"
          className={classes.textField}
        />
        <br />
        <br />
        <TextField
          fullWidth
          label="Password"
          name="password"
          onChange={(e) => updateData(e, inputData, setInputData)}
          type="password"
          size="small"
          className={classes.textField}
        />
        {newUser ? (
          <TextField
            fullWidth
            label="Verify password"
            name="rewpassword"
            onChange={(e) => updateData(e, rewPassword, setRewPassword)}
            type="password"
            size="small"
            className={classes.textField}
          />
        ) : null}
        <br />
        <br />

        <CardActions>
          <Button
            onClick={
              !newUser
                ? toggelUser
                : rewPassword.rewpassword === ''
                ? toggelUser
                : addUser
            }
          >
            Add New User
          </Button>
          {!newUser ? <Button onClick={login}>LogIn</Button> : null}
          <Button onClick={cancel}>Cancel</Button>
        </CardActions>
      </Card>
    </Box>
  );
};

export default LoginModal;
