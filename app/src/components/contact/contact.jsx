import React, { useState, useContext, useEffect } from 'react';
import { TextField, Box, Button, Typography } from '@material-ui/core';
import { Redirect } from 'react-router-dom';

import { AuthContext } from '../../context-API/AuthContext';
import { postContact } from '../../utils/REST-API';

import Header from '../main/header';
import Footer from '../main/footer';

import { text } from '../../templates/text';

import { useStyles } from './style_contact';

const Contact = () => {
  const { auth, mail, location } = useContext(AuthContext);
  const [form, setForm] = useState({
    ip: '',
    name: '',
    mail: '',
    message: '',
    info: {},
  });
  const [redirect, setRedirect] = useState(false);
  const [mess, setMess] = useState({ color: 'black', text: '' });

  const classes = useStyles();

  useEffect(() => {
    setForm({ mail });

    return () => {};
  }, [auth, mail]);

  const formUpdate = (e) => {
    setForm({ ...form, [e.target.name]: e.target.value });
  };
  const send = () => {
    if (form.name && form.mail && form.message) {
      form.info = location;
      postContact(form)
        .then(() => {
          setMess({
            ...mess,
            text: "Thanks! We'll post your ad as soon as we can.",
          });
          setTimeout(() => {
            setMess({ ...mess, text: '' });
            setRedirect(true);
          }, 3000);
        })
        .catch((err) => console.log(err));
    } else {
      setMess({ ...mess, color: 'red', text: 'Must fill in all fields!!' });
      setTimeout(() => {
        setMess({ ...mess, color: 'black', text: '' });
      }, 3000);
    }
  };
  if (redirect) {
    return <Redirect to="/" />;
  }

  return (
    <Box className={classes.contactContainer}>
      <Header btn={true} />
      <Box
        display="flex"
        justifyContent="center"
        className={classes.contactForm}
      >
        <Box>
          <Typography variant="h5">Contact</Typography>
          <br />
          <Typography style={{ width: 400 }}>{text.contact}</Typography>
          <br />
          <br />
          <TextField
            className={classes.textField}
            size="small"
            variant="outlined"
            type="text"
            label="Name"
            name="name"
            onChange={formUpdate}
          />
          <br />
          <br />
          {auth ? (
            <Typography className={classes.mailText}>{mail}</Typography>
          ) : (
            <TextField
              className={classes.textField}
              size="small"
              variant="outlined"
              type="email"
              label="E-mail"
              name="mail"
              onChange={formUpdate}
            />
          )}
          <br />
          <br />
          <TextField
            className={classes.textField}
            size="small"
            variant="outlined"
            type="test"
            multiline
            rows="5"
            label="Message"
            name="message"
            onChange={formUpdate}
          />
          <br />
          <br />
          <Typography style={{ color: mess.color }}>{mess.text}</Typography>
          <Button className={classes.sendBtn} onClick={send}>
            Send
          </Button>
        </Box>
      </Box>
      <Footer />
    </Box>
  );
};

export default Contact;
