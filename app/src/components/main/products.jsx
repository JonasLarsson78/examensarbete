import React, { useEffect, useState, useContext } from 'react';
import { Redirect } from 'react-router-dom';
import { SearchContext } from '../../context-API/SearchContext';
import { AuthContext } from '../../context-API/AuthContext';
import { Box, Button, Container, CardMedia } from '@material-ui/core';
import { getAdverts, getFav } from '../../utils/REST-API';
import { useStyles } from './style_products';
import { getIp } from '../../utils/REST-API';

const Products = () => {
  const [pro, setPro] = useState([]);
  const [fav, setFav] = useState([]);

  const [redirect, setRedirect] = useState(false);
  const [id, setID] = useState(null);
  const { formData } = useContext(SearchContext);
  const { location, setlocation } = useContext(AuthContext);
  const classes = useStyles();
  const query = formData;

  const category = 'category=' + query.category;
  const search = 'search=' + query.search;
  const price = 'price=' + query.price;
  const city = 'city=' + query.city;

  useEffect(() => {
    getAdverts(category + '&' + search + '&' + price + '&' + city).then(
      (res) => {
        setPro(res.data);
      },
    );
  }, [formData, category, search, price, city]);

  useEffect(() => {
    if (!location) {
      getIp()
        .then((res) => {
          setlocation(res.data);
        })
        .catch((err) => console.log(err));
    }
  });

  useEffect(() => {
    getFav()
      .then((res) => {
        setFav(res.data);
      })
      .catch((err) => console.log(err));
  }, []);

  const goToProduct = (e) => {
    setID(e.currentTarget.dataset.id);
    setRedirect(true);
  };

  if (redirect) {
    return (
      <Redirect
        to={{
          pathname: `/product:${id}`,
        }}
      />
    );
  }

  return (
    <Container className={classes.productsContainer}>
      <Box className={classes.productsItems}>
        {pro.length ? (
          pro.map((data) => {
            return (
              <Box className={classes.produckts} key={data._id}>
                <img
                  className={classes.productsImg}
                  alt={data.img}
                  src={`https://jonas78dev-5798.restdb.io/media/${data.img}`}
                />
                <Box>
                  <b>{data.title}</b>
                </Box>
                <Box>{data.price + ' kr'}</Box>
                <Button
                  onClick={goToProduct}
                  data-id={data._id}
                  className={classes.produktsInfoBtn}
                >
                  More Info
                </Button>
              </Box>
            );
          })
        ) : (
          <Box>
            <CardMedia
              alt="img"
              className={classes.manPic}
              image="../images/profile_img.jpg"
            />
            <br />
            <Box style={{ textAlign: 'center' }}>
              <b>Search your flea bargains...</b>
            </Box>
            <Box display="flex">
              {fav.map((data) => {
                return (
                  <Box className={classes.produckts} key={data._id}>
                    <img
                      className={classes.productsImg}
                      alt={data.img}
                      src={`https://jonas78dev-5798.restdb.io/media/${data.img}`}
                    />
                    <Box>
                      <b>{data.title}</b>
                    </Box>
                    <Box>{data.price + ' kr'}</Box>
                    <Button
                      onClick={goToProduct}
                      data-id={data._id}
                      className={classes.produktsInfoBtn}
                    >
                      More Info
                    </Button>
                  </Box>
                );
              })}
            </Box>
          </Box>
        )}
      </Box>
    </Container>
  );
};

export default Products;
