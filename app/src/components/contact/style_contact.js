import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(() => ({
  contactContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100vw',
    height: '100vh',
    background: '#efdebd',
  },
  contactForm: {
    position: 'relative',
    top: 200,
  },
  textField: {
    width: 500,
    marginTop: 1,
    backgroundColor: 'white',
    borderRadius: 4,
    '& label.Mui-focused': {
      //color: '#634b01',
    },
    '& .MuiInput-underline:after': {
      //borderBottomColor: '#634b01',
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        //borderColor: 'red',
      },
      '&:hover fieldset': {
        borderColor: '#634b01',
      },
      '&.Mui-focused fieldset': {
        borderColor: '#634b01',
        border: '1px solid',
      },
    },
  },
  sendBtn: {
    position: 'relative',
    left: 430,
  },
  mailText: {
    position: 'relative',
    left: 10,
  },
}));
