import React from 'react';
import { Box, Typography } from '@material-ui/core';
import { useStyles } from './style_footer';

const Footer = () => {
  const classes = useStyles();
  return (
    <Box className={classes.footerContainer}>
      <Typography className={classes.footerText}>
        Examensarbete - EC utbilding - FEU18 - Jonas Larsson
      </Typography>
    </Box>
  );
};

export default Footer;
