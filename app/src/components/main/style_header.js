import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(() => ({
  headerContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100vw',
    height: 150,
    background: '#e2ca9a',
  },
  headerLogo: {
    width: 200,
    height: 130,
    marginLeft: 30,
    marginTop: 10,
  },
  headerNav: {
    position: 'relative',
    top: -30,
    marginRight: 20,
  },
  headerLoginBtn: {
    position: 'absolute',
    right: 20,
    top: 10,
    '&:disabled': {
      color: '#634b01',
    },
  },
  headerLogOutBtn: {
    position: 'absolute',
    right: 20,
    top: 40,
  },
  headerNoneTextDecoration: {
    textDecoration: 'none',
  },
}));
