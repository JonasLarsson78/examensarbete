import React, { useEffect, useState } from 'react';

import {
  Table,
  TableHead,
  TableContainer,
  TableBody,
  TableCell,
  TableRow,
  Link,
  Button,
  Box,
  Link as A,
} from '@material-ui/core';

import {
  getAllFalse,
  delFlea,
  postTrue,
  getFav,
  delImg,
} from '../utils/REST-API';

import Header from './header';
import Fav from './fav/fav';

import { useStyles } from './admin_style';
import './admin_animation.css';

import Footer from './footer';

const Admin = () => {
  const [list, setList] = useState([]);
  const [favList, setFavList] = useState([]);
  const [load, setLoad] = useState(false);
  const [favorite, setFavorite] = useState(false);
  const [addData, setAddData] = useState(true);
  const classes = useStyles();

  const fixPhoneNumber = (num) => {
    if (num[0] !== '0') {
      let newNum = '0' + num;
      return newNum;
    }

    return num;
  };

  const get = () => {
    setLoad(true);
    getAllFalse()
      .then((res) => {
        const filtered = res.data.filter((x) => x.ok === false);
        setList(filtered);
        setLoad(false);
      })
      .catch((err) => console.log(err));
  };
  const getFavorite = () => {
    getFav()
      .then((res) => {
        setFavList(res.data);
      })
      .catch((err) => console.log(err));
  };

  useEffect(() => {
    if (addData) {
      get();
      getFavorite();
    }

    return () => {
      setAddData(false);
    };
  }, [addData]);

  const change = (e, fun) => {
    const { id, img } = e.currentTarget.dataset;

    fun(id)
      .then(() => {
        if (fun === delFlea) {
          delImg(img);
        }
        get();
      })
      .change((err) => console.log(err));
  };

  return (
    <Box
      display="flex"
      justifyContent="center"
      className={classes.adminContainer}
    >
      <Header
        btn={true}
        get={get}
        favorite={favorite}
        setFavorite={setFavorite}
      />
      <TableContainer className={classes.adminTableContainer}>
        {!favorite ? (
          <Table size="small" className="adminTable">
            <TableHead>
              <TableRow>
                <TableCell>Title</TableCell>
                <TableCell>Category</TableCell>
                <TableCell>Seller</TableCell>
                <TableCell>Mail</TableCell>
                <TableCell>Phone</TableCell>
                <TableCell>Image Preview</TableCell>
                <TableCell>State</TableCell>
                <TableCell></TableCell>
                <TableCell></TableCell>
              </TableRow>
            </TableHead>
            {list.length ? (
              <TableBody>
                {list.map((data) => {
                  return (
                    <TableRow key={data._id}>
                      <TableCell>{data.title}</TableCell>
                      <TableCell>{data.category}</TableCell>
                      <TableCell>{data.seller}</TableCell>
                      <TableCell>
                        <Link
                          href={`mailto:${data.mail}?subject=${data.title} på Flea Market`}
                        >
                          {data.mail}
                        </Link>
                      </TableCell>
                      <TableCell>
                        <Link href={`tel:${fixPhoneNumber(data.phone)}`}>
                          {fixPhoneNumber(data.phone)}
                        </Link>
                      </TableCell>
                      <TableCell>
                        <A
                          href={`https://jonas78dev-5798.restdb.io/media/${data.img}`}
                          target="_blank"
                        >
                          Preview
                        </A>
                      </TableCell>
                      <TableCell>{data.ok ? null : 'Review'}</TableCell>
                      <TableCell>
                        <Button
                          onClick={(e) => change(e, postTrue)}
                          data-id={data._id}
                        >
                          Ok
                        </Button>
                      </TableCell>
                      <TableCell style={{ width: '150px' }}>
                        <Button
                          onClick={(e) => change(e, delFlea)}
                          data-id={data._id}
                          data-img={data.img}
                        >
                          Delete
                        </Button>
                      </TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            ) : (
              <TableBody>
                <TableRow>
                  <TableCell style={{ textAlign: 'center' }} colSpan="8">
                    List is empty. No new adverts...
                  </TableCell>
                </TableRow>
              </TableBody>
            )}
          </Table>
        ) : (
          <Fav fav={favList} getFavorite={getFavorite} setLoad={setLoad} />
        )}
      </TableContainer>
      {load ? (
        <Box
          display="flex"
          justifyContent="center"
          className={classes.adminModal}
        >
          <Box className="loader">
            <Box className={classes.adminLoader}>Loading</Box>
          </Box>
        </Box>
      ) : null}
      <Footer />
    </Box>
  );
};

export default Admin;
