import React, { useContext } from 'react';
import { category } from '../../templates/category';
import { SearchContext } from '../../context-API/SearchContext';

import { Box, Radio, FormLabel, SvgIcon } from '@material-ui/core';
import { styled } from '@material-ui/styles';

import { useStyles } from './style_category';

const Category = () => {
  const { formData, setFormData } = useContext(SearchContext);
  const classes = useStyles();
  const handleChange = (e) => {
    setFormData({ ...formData, category: e.target.value });
  };
  return (
    <Box>
      {category.map((data, index) => {
        const Icon = styled(SvgIcon)({
          color: '#634b01',
        });
        return (
          <span key={index}>
            <Icon
              className={classes.icon}
              fontSize="large"
              component={data.icon}
            />
            <FormLabel>{data.text}</FormLabel>
            <Radio
              checked={formData.category === data.text}
              onChange={handleChange}
              value={data.text}
              color="default"
              name="radio-button-demo"
              size="small"
            />
          </span>
        );
      })}
    </Box>
  );
};

export default Category;
