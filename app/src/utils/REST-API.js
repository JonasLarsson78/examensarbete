import axios from 'axios';

const advertsURL = '/api/adverts';
const advertURL = '/api/advert';
const advertsFav = '/api/fav';
const advertsMail = '/api/mail';
const contactFrom = '/api/contact';
const getRandomImg = '/api/imgrandom';

export const getFav = () => axios.get(`${advertsFav}?fav=true`);

export const getMail = (mail) => axios.get(`${advertsMail}/${mail}`);

export const delFlea = (id) => axios.delete(`${advertURL}/${id}`);

export const getAdverts = (query) => axios.get(`${advertsURL}?${query}`);

export const getAdvert = (id) => axios.get(`${advertURL}/${id}`);

export const postAdverts = (data) => axios.post(advertsURL, data);

export const postContact = (obj) => axios.post(contactFrom, obj);

export const getRanImg = () => axios.get(getRandomImg);

export const getIp = () => axios.get('/ip');

/* export const getIp = () =>
  axios.get('https://cors-anywhere.herokuapp.com/https://v4.ifconfig.co/json'); */

export const postImg = (data) =>
  axios.post(`https://jonas78dev-5798.restdb.io/media`, data, {
    headers: {
      'x-apikey': '5e1c819d4cfae7143cefb101',
      'Content-Type': true,
    },
  });
export const delImg = (id) =>
  axios.delete(`https://jonas78dev-5798.restdb.io/media/${id}`, {
    headers: {
      'x-apikey': '5e1c819d4cfae7143cefb101',
    },
  });
