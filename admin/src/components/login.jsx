import React, { useState, useEffect, useContext } from 'react';
import { TextField, Box, Button, Card, CardMedia } from '@material-ui/core';
import { authWithPassword, init } from '../firebase/firebaseUtils';
import { AuthContext } from '../context-API/AuthContext';
import { checkAdmin } from '../utils/REST-API';

import { useStyles } from './login_style';

const Login = () => {
  const [inputData, setInputData] = useState({ email: '', password: '' });
  const [errorMess, setErrorMess] = useState('');
  const { setAuth } = useContext(AuthContext);

  const classes = useStyles();

  useEffect(() => {
    init();
  }, []);

  const updateInputData = (e) => {
    setInputData({ ...inputData, [e.target.name]: e.target.value });
  };
  const login = () => {
    checkAdmin({ mail: inputData.email }).then((res) => {
      if (res.data) {
        authWithPassword(inputData)
          .then(() => setAuth(true))
          .catch((e) => setErrorMess(e.message));
      } else {
        console.log('Not Admin Login!!');
      }
    });
  };
  return (
    <Box
      className={classes.loginContainer}
      display="flex"
      justifyContent="center"
    >
      <Card raised={true} className={classes.loginCard}>
        <CardMedia
          className={classes.loginCardMedia}
          image="./images/flea-market-logo_white.jpg"
          title="Logo"
        />
        <br />
        <br />
        <TextField
          fullWidth
          label="E-mail"
          name="email"
          onChange={updateInputData}
          type="text"
          className={classes.textField}
        />
        <br />
        <br />
        <TextField
          fullWidth
          label="password"
          name="password"
          onChange={updateInputData}
          type="password"
          className={classes.textField}
        />
        <br />
        <br />
        <Button fullWidth onClick={login}>
          Login
        </Button>
        <div>{errorMess}</div>
      </Card>
    </Box>
  );
};

export default Login;
