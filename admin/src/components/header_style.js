import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme) => ({
  headerContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: 150,
    background: '#80b3ff',
  },
  headerLogo: {
    width: 200,
    marginLeft: 30,
    marginTop: 10,
  },
  headerUpdateBtn: {
    position: 'absolute',
    right: 100,
    top: 100,
  },
  headerLogoutBtn: {
    position: 'absolute',
    right: 20,
    top: 100,
  },
  headerMessBtn: {
    position: 'absolute',
    top: 100,
  },
  padding: {
    padding: theme.spacing(0, 1.5),
  },
}));
