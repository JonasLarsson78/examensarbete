import React from 'react';
import { Box } from '@material-ui/core';
import { useStyles } from './footer_style';

const Footer = () => {
  const classes = useStyles();
  return (
    <Box className={classes.footerContainer}>
      <Box textAlign="center" paddingTop={5} className={classes.footerText}>
        Examensarbete - EC utbilding - Frontend - Jonas Larsson
      </Box>
    </Box>
  );
};

export default Footer;
