import React, { useEffect, useState } from 'react';
import ReactCountryFlag from 'react-country-flag';
import {
  Box,
  Button,
  Table,
  TableHead,
  TableCell,
  TableRow,
  TableBody,
  Link as A,
} from '@material-ui/core';
import {
  Person,
  AlternateEmail,
  Message as Mess,
  DeleteForever,
  Dns,
  Map,
} from '@material-ui/icons';

import { useStyles } from './style_message';

import { Link, Redirect } from 'react-router-dom';
import Header from '../header';
import Footer from '../footer';

import { getMess, postMessOld, delMess } from '../../utils/REST-API';

const Message = ({ match }) => {
  const [message, setMessage] = useState([]);
  const [addData, setAddData] = useState(true);
  const [mail, setMail] = useState('');
  const [redirect, setRedirect] = useState(false);
  const [info, setInfo] = useState(false);

  const classes = useStyles();

  const id = match.params.id.substr(1);

  useEffect(() => {
    if (addData) {
      getMess(id)
        .then((res) => {
          postMessOld(id);
          setMessage(res.data);
          setMail(res.data[0].mail);
        })
        .catch((err) => console.log(err));
    }
    return () => {
      setAddData(false);
    };
  }, [id, addData]);
  const del = () => {
    delMess(id)
      .then(() => {
        setRedirect(true);
      })
      .catch((err) => console.log(err));
  };
  if (redirect) {
    return <Redirect to="/list" />;
  }
  return (
    <Box className={classes.messageContainer}>
      <Header btn={false} />
      <Box className={classes.itemContainer}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell className={classes.tableHeadCell}>
                <Person className={classes.iconPos} /> Name:
              </TableCell>
              <TableCell className={classes.tableHeadCell}>
                <AlternateEmail className={classes.iconPos} /> Mail:
              </TableCell>
              <TableCell className={classes.tableHeadCell}>
                <Dns className={classes.iconPos} /> IP:
              </TableCell>
              <TableCell className={classes.tableHeadCell}>
                <Map className={classes.iconPos} />
                Map:
              </TableCell>
              <TableCell className={classes.tableHeadCell}>
                <DeleteForever className={classes.iconPos} />
                Delete
              </TableCell>
            </TableRow>
          </TableHead>
          {message.map((data) => {
            return (
              <TableBody key={data._id}>
                <TableRow>
                  <TableCell className={classes.tableBodyCell}>
                    {data.name}
                  </TableCell>
                  <TableCell className={classes.tableBodyCell}>
                    {data.mail}
                  </TableCell>
                  <TableCell className={classes.tableBodyCell}>
                    <ReactCountryFlag
                      className={classes.ReactCountryFlag1}
                      countryCode={data.info.country}
                      svg
                    />
                    <span
                      className={classes.spanHover}
                      onMouseOver={() => setInfo(true)}
                      onMouseOut={() => setInfo(false)}
                    >
                      {data.info.ip}
                    </span>
                  </TableCell>
                  <TableCell className={classes.tableBodyCell}>
                    {data.info.ll ? (
                      <A
                        className={classes.mapLink}
                        href={`https://www.google.com/maps/@${data.info.ll[0]},${data.info.ll[1]}`}
                        target="_blank"
                        title={data.info.city}
                        underline="none"
                      >
                        position
                      </A>
                    ) : null}
                  </TableCell>
                  <TableCell className={classes.tableBodyCell}>
                    <Button onClick={del}>Delete</Button>
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell className={classes.tableHeadCell} colSpan="5">
                    <Mess className={classes.iconPos} /> Message:
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell className={classes.tableBodyCell} colSpan="5">
                    {data.message}
                  </TableCell>
                </TableRow>
              </TableBody>
            );
          })}
        </Table>
        <Link className={classes.backBtn} to="/list">
          <Button>Back</Button>
        </Link>
        <A underline="none" className={classes.resBtn} href={`mailto:${mail}`}>
          <Button>Response</Button>
        </A>
      </Box>
      <Footer />
      {info ? (
        <Box className={classes.infoModal}>
          <Table>
            {message.map((data, index) => {
              return (
                <TableBody key={index}>
                  <TableRow>
                    <TableCell>
                      <b>IP: </b>
                      {data.info.ip}
                      <b className={classes.bName}> Country: </b>
                      {data.info.country}
                      <ReactCountryFlag
                        className={classes.ReactCountryFlag2}
                        countryCode={data.info.country}
                        svg
                      />
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>
                      <b>City: </b>
                      {data.info.city}
                      <b className={classes.bName}> TimeZone: </b>
                      {data.info.timezone}
                    </TableCell>
                  </TableRow>
                  <TableRow></TableRow>
                </TableBody>
              );
            })}
          </Table>
          <Box className={classes.infoBack}></Box>
        </Box>
      ) : null}
    </Box>
  );
};

export default Message;
