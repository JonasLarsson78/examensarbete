import React, { useContext } from 'react';
import { useStyles } from './style_main';
import { Box, Button } from '@material-ui/core';
import { AuthContext } from '../../context-API/AuthContext';

import Header from './header';
import Search from './search';
import Products from './products';
import Footer from './footer';

const Main = () => {
  const { loginTime } = useContext(AuthContext);
  const classes = useStyles();
  return (
    <Box className={classes.mainContainer}>
      {loginTime !== '' ? (
        <>
          <Box className={classes.loginTimeContainer}></Box>
          <Box className={classes.loginTimeItem}>
            {loginTime}
            <br />
            <br />
            <Button onClick={() => window.location.reload()}>Ok</Button>
          </Box>
        </>
      ) : null}
      <Header btn={true} />
      <Search />
      <Products />
      <Footer />
    </Box>
  );
};

export default Main;
