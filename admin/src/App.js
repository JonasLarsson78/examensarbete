import React, { useContext } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Admin from './components/admin';
import Login from './components/login';
import { AuthContext } from './context-API/AuthContext';
import MessageList from './components/message/messageList';
import Message from './components/message/message';

import './App.css';

function App() {
  const { auth } = useContext(AuthContext);

  return (
    <div className="App">
      {auth ? (
        <>
          <Router>
            <Route exact path="/" component={Admin} />
            <Route path="/list" component={MessageList} />
            <Route path="/message:id" component={Message} />
          </Router>
        </>
      ) : (
        <Login />
      )}
    </div>
  );
}

export default App;
