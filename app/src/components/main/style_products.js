import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(() => ({
  productsContainer: {
    position: 'relative',
    top: 170,
  },
  productsItems: {
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap',
  },
  produckts: {
    width: 150,
    height: 150,
    margin: 20,
    padding: 5,
    textAlign: 'center',
    background: 'white',
    border: '1px solid black',
    borderRadius: 3,
  },
  productsImg: {
    width: 100,
    height: 100,
  },
  produktsInfoBtn: {
    marginLeft: 5,
    marginTop: 3,
    padding: 3,
    width: 100,
    border: 'none',
    background: 'green',
    color: 'white',
    borderRadius: 3,
    cursor: 'pointer',
    '&:hover': {
      background: '#01bb01',
    },
  },
  manPic: {
    width: '250px',
    height: '250px',
    position: 'relative',
    left: '50%',
    transform: 'translate(-50%, 0)',
  },
}));
